package multicados.common.domain;

import jakarta.persistence.Embedded;
import jakarta.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.Instant;
import java.util.Optional;
import multicados.common.domain.embeddable.Audit;
import nh.core.domain.AuditableResource;

@MappedSuperclass
public abstract class AuditablePermanentEntity<S extends Serializable> extends
    PermanentEntity<S> implements AuditableResource<String, Operator, Instant> {

  @Embedded
  private Audit audit;

  protected AuditablePermanentEntity() {
    audit = new Audit();
  }

  public Audit getAudit() {
    return audit;
  }

  public void setAudit(Audit audit) {
    this.audit = Optional.ofNullable(audit).orElse(new Audit());
  }

  @Override
  public Instant getCreatedTimestamp() {
    return audit.getCreatedTimestamp();
  }

  @Override
  public void setCreatedTimestamp(Instant timestamp) {
    audit.setCreatedTimestamp(timestamp);
  }

  @Override
  public Operator getCreator() {
    return audit.getCreator();
  }

  @Override
  public void setCreator(Operator creator) {
    audit.setCreator(creator);
  }

  @Override
  public Instant getUpdatedTimestamp() {
    return audit.getUpdatedTimestamp();
  }

  @Override
  public void setUpdatedTimestamp(Instant timestamp) {
    audit.setUpdatedTimestamp(timestamp);
  }

  @Override
  public Operator getUpdater() {
    return audit.getUpdater();
  }

  @Override
  public void setUpdater(Operator updater) {
    audit.setUpdater(updater);
  }
}
