package multicados.common.domain.embeddable;

import static multicados.common.domain.mapping.Identifiable.ID;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.io.IOException;
import java.io.Serial;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import multicados.common.domain.Operator;
import multicados.common.domain.mapping.Auditable;
import nh.core.domain.DomainComponent;

@Embeddable
public class Audit implements DomainComponent, Serializable {

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = Auditable.CREATOR, referencedColumnName = ID, updatable = false)
  private Operator creator;

  @ManyToOne(fetch = FetchType.LAZY, optional = false)
  @JoinColumn(name = Auditable.UPDATER, referencedColumnName = ID)
  private Operator updater;

  @Column(name = Auditable.CREATED_TIMESTAMP, nullable = false, updatable = false)
  private Instant createdTimestamp;

  @Column(name = Auditable.UPDATED_TIMESTAMP, nullable = false)
  private Instant updatedTimestamp;

  public Audit() {
  }

  public Audit(Operator creator, Operator updater, Instant createdTimestamp,
      Instant updatedTimestamp) {
    this.creator = creator;
    this.updater = updater;
    this.createdTimestamp = createdTimestamp;
    this.updatedTimestamp = updatedTimestamp;
  }

  public Operator getCreator() {
    return creator;
  }

  public void setCreator(Operator creator) {
    this.creator = creator;
  }

  public Operator getUpdater() {
    return updater;
  }

  public void setUpdater(Operator updater) {
    this.updater = updater;
  }

  public Instant getCreatedTimestamp() {
    return createdTimestamp;
  }

  public void setCreatedTimestamp(Instant createdTimestamp) {
    this.createdTimestamp = createdTimestamp;
  }

  public Instant getUpdatedTimestamp() {
    return updatedTimestamp;
  }

  public void setUpdatedTimestamp(Instant updatedTimestamp) {
    this.updatedTimestamp = updatedTimestamp;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Audit audit = (Audit) o;

    if (!Objects.equals(creator, audit.creator)) {
      return false;
    }
    if (!Objects.equals(updater, audit.updater)) {
      return false;
    }
    if (!Objects.equals(createdTimestamp, audit.createdTimestamp)) {
      return false;
    }
    return Objects.equals(updatedTimestamp, audit.updatedTimestamp);
  }

  @Override
  public int hashCode() {
    int result = creator != null ? creator.hashCode() : 0;
    result = 31 * result + (updater != null ? updater.hashCode() : 0);
    result = 31 * result + (createdTimestamp != null ? createdTimestamp.hashCode() : 0);
    result = 31 * result + (updatedTimestamp != null ? updatedTimestamp.hashCode() : 0);
    return result;
  }

  @Serial
  private void writeObject(java.io.ObjectOutputStream stream)
      throws IOException {
    stream.defaultWriteObject();
  }

  @Serial
  private void readObject(java.io.ObjectInputStream stream)
      throws IOException, ClassNotFoundException {
    stream.defaultReadObject();
  }

}
