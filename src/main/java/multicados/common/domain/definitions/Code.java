package multicados.common.domain.definitions;

/**
 * @author Ngoc Huy
 */
public abstract class Code {

  public static final int LENGTH = 10;

  private Code() {
    throw new UnsupportedOperationException();
  }

}
