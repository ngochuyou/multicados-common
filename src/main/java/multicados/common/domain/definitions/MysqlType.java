package multicados.common.domain.definitions;

/**
 * @author Ngoc Huy
 */
public abstract class MysqlType {

  public static final String BIGINT = "BIGINT";

  private MysqlType() {
    throw new UnsupportedOperationException();
  }

}
