package multicados.common.domain.definitions;

/**
 * @author Ngoc Huy
 */
public abstract class Generator {

  public static final String TABLE_GENERATOR = "shared_table_generator";
  public static final String TABLE_GENERATOR_TABLE_NAME = "id_generators";
  private Generator() {
    throw new UnsupportedOperationException();
  }

  public abstract static class Base32 {

    public static final int CROCKFORD_1A = 42;
    public static final int CROCKFORD_10A = 1034;
    private Base32() {
      throw new UnsupportedOperationException();
    }

  }

}
