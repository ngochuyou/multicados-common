package multicados.common.domain;

import jakarta.persistence.MappedSuperclass;
import java.io.Serializable;
import nh.core.domain.Entity;
import nh.core.domain.PermanentResource;
import nh.core.domain.annotation.Existence;

/**
 * @author Ngoc Huy
 */
@MappedSuperclass
public abstract class PermanentEntity<S extends Serializable>
    implements PermanentResource, Entity<S> {

  @Existence
  private boolean deleted;

  @Override
  public boolean isDeleted() {
    return deleted;
  }

  @Override
  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }
}
