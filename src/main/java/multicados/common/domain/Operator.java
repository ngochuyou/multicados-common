package multicados.common.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import nh.core.domain.IdentifiableResource;

@Entity
@Table(name = "operators")
public class Operator extends User implements IdentifiableResource<String> {

}
