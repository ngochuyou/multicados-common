package multicados.common.domain.mapping;

public abstract class Identifiable {

  private Identifiable() {
    throw new UnsupportedOperationException();
  }

  public static final String ID = "id";

}
