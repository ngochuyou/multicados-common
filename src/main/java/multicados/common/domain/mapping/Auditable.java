package multicados.common.domain.mapping;

public abstract class Auditable {

  private Auditable() {
    throw new UnsupportedOperationException();
  }

  public static final String CREATOR = "creator";
  public static final String UPDATER = "updater";
  public static final String CREATED_TIMESTAMP = "created_timestamp";
  public static final String UPDATED_TIMESTAMP = "updated_timestamp";

}
